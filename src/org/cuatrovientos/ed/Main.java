package org.cuatrovientos.ed;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Product p0=new Product("manzanas", 5, 12.1F);
		Product p1=new Product("alubias", 50, 1.1F);
		Product p2=new Product("Pan", 2, 2.1F);

		Invoice factura= new Invoice("Ander");
		factura.add(p0);
		factura.add(p1);
		factura.add(p2);
		
		float precioTotal= factura.total();
		System.out.println("El precio total es: "+ precioTotal);
		
		factura.remove(1);
		
		System.out.println("El nuevo precio es:"+ factura.total());
	}
	
}
