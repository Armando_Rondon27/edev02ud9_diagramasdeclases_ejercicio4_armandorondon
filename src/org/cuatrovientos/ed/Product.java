package org.cuatrovientos.ed;

public class Product {
	 private String name;
	 private int qty;
	 private float price;
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(String name, int qty, float price) {
		super();
		this.name = name;
		this.qty = qty;
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [name=" + name + ", qty=" + qty + ", price=" + price + "]";
	}
	 
	public float total() {
		return this.price * this.qty;
		
	}
	
	 
	 
	 
	 
	 
	 
}
